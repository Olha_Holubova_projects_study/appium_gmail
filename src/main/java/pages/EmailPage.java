package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EmailPage extends BasePage {

    @FindBy(xpath = "//div[@class = 'il d v']")
    WebElement createButton;

    @FindBy(xpath = "//div[contains(text(),'Отправлено')]")
    WebElement sendMessage;

    public EmailPage(AppiumDriver appiumDriver) {
        super(appiumDriver);
    }

    public MessagePage createMessage() {
        click(createButton);
        return new MessagePage(driver);
    }

    public boolean senMessageDisplayed()
    {
        return sendMessage.isDisplayed();
    }


}
