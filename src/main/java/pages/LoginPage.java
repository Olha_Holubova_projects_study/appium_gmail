package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage{

    private static final String SITE = "https://gmail.com";

    @FindBy(xpath = "//input[@type = 'email']")
    private WebElement email;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement password;

    public LoginPage(AppiumDriver appiumDriver) {
        super(appiumDriver);
    }

    public LoginPage openPage() {
        driver.get(SITE);
        return this;
    }

    public EmailPage logInWithValidCredentials(String emailText, String passwordText) {
        sendText(email, emailText + "\n");
        sendText(password, passwordText + "\n");
        return new EmailPage(driver);
    }

}
